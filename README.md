# Conventions



## Getting started

![](181304.png)


## Comparison
```
data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
        override fun compareTo(other: MyDate): Int = when {
        year > other.year -> 1
        year < other.year -> -1
        month > other.month -> 1
        month < other.month -> -1
        dayOfMonth > other.dayOfMonth -> 1
        dayOfMonth < other.dayOfMonth -> -1
        else -> 0
    }
}

fun test(date1: MyDate, date2: MyDate) {
    // this code should compile:
    println(date1 < date2)
}
```

## Ranges
```
fun checkInRange(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return date in first..last
}
```

## For loop
```
class DateRange(val start: MyDate, val end: MyDate): Iterable<MyDate> {
    override fun iterator(): Iterator<MyDate> {
         return object : Iterator<MyDate> {
            var currentDate = start
            override fun hasNext(): Boolean {
                return currentDate <= end
            }

            override fun next(): MyDate {
                val retain = currentDate
                currentDate = currentDate.followingDate()
                return retain
            }
        }
    }
}

fun iterateOverDateRange(firstDate: MyDate, secondDate: MyDate, handler: (MyDate) -> Unit) {
    for (date in firstDate..secondDate) {
        handler(date)
    }
}
```

## Operators overloading
```
import TimeInterval.*

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)

enum class TimeInterval { DAY, WEEK, YEAR }

operator fun MyDate.plus(timeInterval: TimeInterval): MyDate =
        this.addTimeIntervals(timeInterval, 1)

operator fun TimeInterval.times(number: Int): TimeIntervals
        = TimeIntervals(this, number)

operator fun MyDate.plus(timeIntervals: TimeIntervals): MyDate =
        this.addTimeIntervals(timeIntervals.timeInterval, timeIntervals.number)

data class TimeIntervals(val timeInterval: TimeInterval, val number: Int)

fun task1(today: MyDate): MyDate {
    return today + YEAR + WEEK
}

fun task2(today: MyDate): MyDate {
    return today + YEAR * 2 + WEEK * 3 + DAY * 5
}
```

## Invoke
```
class Invokable {
    var numberOfInvocations: Int = 0
        private set

    operator fun invoke(): Invokable {
        numberOfInvocations++
        return this
    }
}

fun invokeTwice(invokable: Invokable) = invokable()()
```
